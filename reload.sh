#!/bin/bash

#Stop & delete the latest containers
containersId=`docker ps -ql`
if [ ${#containersId} -gt 11 ]; then
	docker stop $containersId
	docker rm $containersId
	echo "(latest container deleted)"
else
	echo "(no containers left)"
fi

#relaunch the wanted container
if [ $# -eq 1 ]; then
	case "$1" in
		#help info
		"help") echo "HELP : Stop and delete the latest container."
			echo "Take 1 argument, the container's designation to refresh. \`./reload.sh l\` to get the list of designations available."
		;;
		#designation list
		"l") echo "learnit : interactive-tutorial repo : learnshell.org"
		;;
		#interactive-tutorial repo
		"learnit") 
cd /Users/tatadmound/Documents/Dev/2021/docker/githubs/interactive-tutorials | make build && make run
		;;
	esac
elif [ $# -gt 1 ]; then
	echo "too much arguments, only 1 needed"
fi
